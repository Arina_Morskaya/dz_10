from collections import deque
import random
import time


class Proxy:
    def __init__(self, ip, proxiesCounter):
        self.ip = ip
        self.port = proxiesCounter[ip][0]
        self.date_used = proxiesCounter[ip][1]
        self.counter = proxiesCounter[ip][2]
        self.status = proxiesCounter[ip][3]

    def __str__(self):
        return 'IP: {}, Port: {}, Last Date used: {}, Counter: {}, ' \
               'Status: {}'.format(self.ip, self.port, self.date_used, self.counter, self.status)


class ProxyManager:
    def __init__(self):
        self.proxiesCounter = {}
        self.workingProxies = []
        self.bannedProxies = []
        self.proxies = self.read_file()
        self.proxiesQueue = deque(self.proxies)

    def read_file(self):
        with open('D:/ipPort.txt', 'r') as f:
            self.proxies = [s.strip() for s in f.readlines()]
        return self.proxies

    def next_proxy(self):
        new_proxy = self.proxiesQueue.popleft()
        if new_proxy in self.workingProxies:
            self.workingProxies.remove(new_proxy)
        return new_proxy

    def back_proxy(self, proxy, response):
        self.counter(proxy, response)
        if response == 'Ok':
            self.workingProxies.append(proxy)
            self.proxiesQueue.append(proxy)
        else:
            self.bannedProxies.append(proxy)

    def counter(self, proxy, response):
        ip = proxy.split(':')[0]
        port = proxy.split(':')[1]
        if ip in self.proxiesCounter.keys():
            self.proxiesCounter[ip][0] = port
            self.proxiesCounter[ip][1] = time.time()
            self.proxiesCounter[ip][2] += 1
            self.proxiesCounter[ip][3] = response
        else:
            self.proxiesCounter[ip] = [port, time.time(), 1, response]

    def make_request(self):
        try:
            proxy = self.next_proxy()
        except IndexError:
            print('Proxies are over!!!')
        else:
            """request"""
            response = random.choice(('Ok', 'Banned'))
            self.back_proxy(proxy, response)


p = ProxyManager()
for i in range(50):
    p.make_request()
    time.sleep(0.01)

for key, value in p.proxiesCounter.items():
    print('Ip: {}, Port: {}, Date: {}, Counter: {}, '
          'Status: {}, '.format(key, value[0], value[1], value[2], value[3]))

pr = Proxy('205.169.145.131', p.proxiesCounter)
print('-------------------------------------------------------------')
print(pr)
