class Car:
    def __init__(self, volume, mileage, color, max_speed, max_fuel, price):
        self.volume = volume
        self.mileage = mileage
        self.color = color
        self.max_speed = max_speed
        self.max_fuel = max_fuel
        self.price = price

    def __lt__(self, other):
        if (self.price, self.volume, self.max_fuel) < (other.price, other.volume, other.max_fuel):
            return True

    def __repr__(self):
        return 'Volume engine: {}, Mileage: {}, Color: {}, Max speed: {}, Max fuel: {}, Price: {}'.format\
            (self.volume, self.mileage, self.color, self.max_speed, self.max_fuel, self.price)


cars = [Car(1.5, 10000, 'red', 260, 70, 4000),
        Car(3.0, 78000, 'white', 190, 55, 2600),
        Car(2.8, 12000, 'blue', 240, 60, 1000),
        Car(2.6, 5000, 'black', 200, 50, 2600),
        Car(1.8, 10000, 'gray', 180, 55, 1600),
        Car(3.0, 10000, 'gray', 180, 65, 2600),
        Car(1.8, 10000, 'gray', 180, 65, 1600),
        Car(1.1, 10000, 'gray', 180, 55, 1000)]

for i in sorted(cars):
    print(i)
